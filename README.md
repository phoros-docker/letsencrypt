# Let's Encrypt using acme.sh 
Using [acme.sh](https://github.com/Neilpang/acme.sh) as an issuing client for Let'sEncrypt certificates

### Important
This image uses a directory to write ACME challenges into but it does not serve it to the outside world to keep the image size small. In other words: it doesn't have a webserver so you need to use your own. In order to issue certificates you need to mount a directory as `/var/www/letsencrypt/` into this container which is handled by a webserver. Use `--mount type=mount,source=</path/to/your/webserver/directory/>,destination=/var/www/letsencrypt/` in your Docker command to achieve this. Or use a Docker volume to share it across containers without using the host (cleaner approach).

### Environment variables
| Variable | Type | Purpose |
| -------- | ---- | ------- |
| CERT_DIRECTORY | String [unix path] | The directory where certificates are stored. The default value is `/etc/letsencrypt/`. Leave it as it is as there is no need to change it. |
| DOMAINS | String [domain] | A space separated list of domains to issue certificates for. To issue a multi-domain certificate use a colon separated list. Make sure your main domain is the first entry in the list in order to mount the directory of the issued certificate into other containers. The path for certificates is `$CERT_DIRECTORY/<main domain>/` with the usual Let's Encrypt file names (`ca.pem`, `fullchain.pem` and `privkey.key`). For example use `example.com backup.example.com` for two independent certificates or `example.com:web.example.com:sub.example.com` for a multi-domain certificate. You can also mix those two to create more specific certificates. You can enable OCSP for specific certificates when you add `:OCSP` at the end of each certificate group, for example `ocsp.example.com:OCSP noocsp.example.com`. Pretty nice, huh? | 
| MAIL_ADDRESS | String [mail address] | The mail address for certificate notifications by Let's Encrypt. |
| CERT_KEYLENGTH | String [2048, 3072, 4096, 8192, ec-256, ec-384] | The keylength to use for certificates (for ECC certificates use *ec-256* or *ec-384*). |
| USE_TESTING | Boolean [true, false] | Whether to use testing certificates or not (see [here](https://letsencrypt.org/docs/staging-environment/) for more information about rate limits). |
| USE_OCSP | Boolean [true, false] | Whether to use OCSP by setting the Must-Staple flag or not (see [here](https://scotthelme.co.uk/ocsp-must-staple/) for more information). **Do note that this flag will enable OCSP for all certificates** and will override OCSP entries in `DOMAINS` |
| REMOVE_CERT_PRIVATE_KEY | Boolean [true, false] | Setting this flag to `true` removes the private key of all certificates listed in `DOMAINS`. acme.sh regenerates any missing private keys upon renewal. Setting this flag will force the renewal of certificates to ensure the existence of a private key file. |
| FORCE_RENEW | Boolean [true, false] | Setting this flag to `true` forces acme.sh to renew certificates during container startup (and only during the first container startup, not during subsequent container restarts). If the current certificate is still valid (newer than 60 days) this flag has no effect. Only use it if the list of domains has changed - set it to `false` or clear it afterwards. |
| CA_CERT_FILENAME | String [filename] | Filename of the CA certificate. Defaults to `ca.pem`. |
| CERT_FILENAME | String [filename] | Filename of the resulting server certificate. Defaults to `fullchain.pem`. |
| PRIVATE_KEY_FILENAME | String [filename] | Filename of the private key file. Defaults to `privkey.key`. |
| DEBUG | Boolean [true, false] | Enable debug output of acme.sh. |
| VERBOSE | Boolean [true, false] | Enable verbose output of acme.sh. |
